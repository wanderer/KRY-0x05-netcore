﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace KRY_0x05_netcore
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int[] otp;
        char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();


        void clear_fields()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            clear_otp();
        }
        void clear_otp()
        {
            if (otp != null)
            {
                for (int i = 0; i < 1337; i++)
                {
                    byte[] random = new byte[1337];
                    random = gimme_bytes(random.Length);
                    otp[i % otp.Length] = Math.Abs(BitConverter.ToInt32(random, 0) % 26);
                }
            }
            otp = null;
        }

        void empty_message_warning()
        {
            MessageBox.Show("The key is to be of the exact same length as the message.\nNo message means no key length to incur.", "Empty message yo");
            return;
        }

        public byte[] gimme_bytes(int howmuch)
        {
            RNGCryptoServiceProvider rnd = new RNGCryptoServiceProvider();
            byte[] buffer = new byte[howmuch];
            rnd.GetBytes(buffer);
            return buffer;
        }

        void gen_nice_key()
        {
            if (textBox1.Text == "")
            {
                empty_message_warning();
                textBox2.Text = "";
                return;
            }
            Match match = Regex.Match(textBox1.Text, @"\d|\s+");
            if (match.Success)
            {
                label6.Visible = true;
            }
            else
            {
                label6.Visible = false;
            }
            string m = textBox1.Text;
            m = m.ToUpper();
            /* classic misdirection */
            m = Regex.Replace(m, @" ", "MEZERABRO");
            m = Regex.Replace(m, @"\d|\s+", "");
            textBox2.Text = m;

            byte[] random = new byte[1337];

            int mlength = textBox2.Text.Length;
            otp = new int[mlength];
            for (int i = 0; i < mlength; i++)
            {
                //rng.GetBytes(random);
                random = gimme_bytes(random.Length);
                otp[i] = Math.Abs(BitConverter.ToInt32(random, 0) % 26);
            }
            textBox3.Text = "";
            for (int i = 0; i < otp.Length; i++)
            {
                textBox3.Text += alphabet[otp[i]];
            }
        }

        void encrypt()
        {
            if (textBox1.Text == "")
            {
                empty_message_warning();
                return;
            }
            else if (otp == null)
            {
                MessageBox.Show("Please regenerate the key");
                return;
            }

            MessageBox.Show("The key **WILL DISAPPEAR** the instant you click OK and you will never see it here again.\nMake sure you write it down (or that you remember it) if you're interested in ever decrypting your precious message.", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            string m = textBox2.Text;
            int[] num_val = new int[m.Length];
            for (int i = 0; i < m.Length; i++)
            {
                string a = m.Substring(i, 1);
                for (int j = 0; j < alphabet.Length; j++)
                {
                    if (a == Convert.ToString(alphabet[j]))
                    {
                        num_val[i] = j;
                    }
                }
            }
            string cryptmessage = "";
            for (int i = 0; i < otp.Length; i++)
            {
                cryptmessage += alphabet[(num_val[i] + otp[i]) % 26];
            }
            textBox3.Text = "";
            clear_otp();
            textBox4.Text = cryptmessage;
        }

        void check_d_key()
        {
            string k = textBox7.Text;
            Match match = Regex.Match(k, @"\d|\s+");
            if (match.Success)
            {
                MessageBox.Show("Invalid characters in the key, not even attempting decryption.\nOnly the letters of the English alphabet are allowed for the key.","Error");
                return;
            }
            return;
        }

        string replace_stuff_d_key(string message)
        {
            return message = Regex.Replace(message, @"MEZERABRO", " ");
        }

        int realmodbro(int x, int m)
        {
            /* solving c#'s remainder instead of modulo */
            int r = x % m;
            return r < 0 ? r + m : r;
        }


        void decrypt()
        {
            if (textBox5.Text == "")
            {
                MessageBox.Show("Nothing to decrypt", "Error");
                return;
            }
            else if (textBox7.Text == "")
            {
                MessageBox.Show("No key provided","Error");
                return;
            }

            check_d_key();
            //string k = replace_stuff_d_key();
            string cryptmessage = textBox5.Text.ToUpper();
            string message = "";
            string k = textBox7.Text.ToUpper();
            int klength = k.Length;
            int clength = cryptmessage.Length;

            otp = new int[klength];
            
            //textBox3.Text = "";
            

            int[] num_val = new int[clength];
            for (int i = 0; i < clength; i++)
            {
                string a = cryptmessage.Substring(i, 1);
                for (int j = 0; j < alphabet.Length; j++)
                {
                    if (a == Convert.ToString(alphabet[j]))
                    {
                        num_val[i] = j;
                    }
                }
            }
            int[] otp_num_val = new int[klength];
            for (int i = 0; i < klength; i++)
            {
                string a = k.Substring(i, 1);
                for (int j = 0; j < alphabet.Length; j++)
                {
                    if (a == Convert.ToString(alphabet[j]))
                    {
                        otp_num_val[i] = j;
                    }
                }
            }
            otp = otp_num_val;

            for (int i = 0; i < otp.Length; i++)
            {
                message += alphabet[realmodbro(num_val[i] - otp[i],26)];
            }

            message = replace_stuff_d_key(message);
            textBox6.Text = message;
            otp_num_val = null;
            otp = null;
            return;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            gen_nice_key();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            clear_fields();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            /* encrypt */
            encrypt();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            /* decrypt */
            decrypt();
        }
    }
}
